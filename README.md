To start server:
1. cd server
2. npm i
3. nodemon app.js

To start client:
1. cd chat-app
2. npm i
3. npm run serve


The client server ip is currently localhost. 
This means the chat will only work for clients on the same machine as the server.
 
Fix:
Change client server ip to the ip the server is running on.