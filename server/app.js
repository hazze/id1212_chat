const express = require('express');
const socket = require('socket.io');

const app = express();
const server = app.listen(4444, () => {
    console.log('Server started! Listening to 4444..');
});

const io = socket(server);
io.on('connection', socket => {
	console.log("New Connection! Socket id: " + socket.id);
    socket.on('SEND_MESSAGE', function(data) {
        io.emit('MESSAGE', data)
	});
	
	socket.on('WRITING_MESSAGE', data => {
        socket.broadcast.emit('WRITING_MESSAGE', data);
    });
});